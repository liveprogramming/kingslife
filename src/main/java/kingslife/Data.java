package kingslife;

import javafx.scene.transform.Scale;

public class Data {
  public static final int SCREEN_WIDTH = 860;
  public static final int SCREEN_HEIGHT = 560;

  public static boolean fullscreen = true;

  public static final Scale scale = new Scale();

  public static final long UPDATE_TIME = 31;

  public static final boolean USE_JHUDP = true;

  public  static final String MAP_PATH = "map/Map.level";
}
