package kingslife.GameObjects;

public abstract class BackgroundObject extends GameObject {

  public BackgroundObject() {
    super();
  }
  public BackgroundObject(int x, int y) {
    super(x,y);
  }
  public BackgroundObject(int x, int y, int width, int height) {
    super(x, y, width, height);
  }

}
