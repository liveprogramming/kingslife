package kingslife.GameObjects;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import kingslife.Data;
import kingslife.gfx.ImageFactory;
import kingslife.gfx.Looks;

import java.util.Random;

public class BackgroundPlayer extends GameObject  {

  private ImageView image;

  private Timeline sprites;
  private TranslateTransition ft;

  public BackgroundPlayer() {
    super();
    width.set(50);
    height.set(50);

    initialize();
  }
  private void initialize() {
    Image player1, player2;
    float startx, endx;

    Random random = new Random();

    boolean leftright = random.nextBoolean(); // true = left; false = right

    // choose left or right
    if(leftright) { // left
      player1 = ImageFactory.newInstance().getLooks(Looks.MENUPLAYER_LEFT_1);
      player2 = ImageFactory.newInstance().getLooks(Looks.MENUPLAYER_LEFT_2);

      startx = Data.SCREEN_WIDTH+width.get();
      endx = 0;
    } else {
      player1 = ImageFactory.newInstance().getLooks(Looks.MENUPLAYER_RIGHT_1);
      player2 = ImageFactory.newInstance().getLooks(Looks.MENUPLAYER_RIGHT_2);

      startx = 0;
      endx = Data.SCREEN_WIDTH+width.get();
    }

    // setup image
    image = new ImageView(player1);
    image.fitWidthProperty().bind(width);
    image.fitHeightProperty().bind(height);
    image.setY(0);
    image.setX(0);

    // setup keyframe for animation
    KeyFrame keyFrames[] = new KeyFrame[2];
    keyFrames[0] = new KeyFrame(Duration.millis(250), e->image.setImage(player1));
    keyFrames[1] = new KeyFrame(Duration.millis(500), e->image.setImage(player2));

    // running-animation
    sprites = new Timeline();
    sprites.setCycleCount(Animation.INDEFINITE);
    sprites.getKeyFrames().setAll(keyFrames);
    sprites.play();

    // actually the running
    x.set(width.get()*(-1));
    y.set(random.nextInt(Data.SCREEN_HEIGHT - (int)height.get()));
    ft = new TranslateTransition(Duration.millis(6000), myGroup);
    ft.setFromX(startx);
    ft.setToX(endx);
    ft.setCycleCount(1);
    ft.setAutoReverse(true);
    ft.play();
    ft.setOnFinished(e->{
      myGroup.getChildren().remove(image);
      myGroup.setVisible(false);
    });

    myGroup.getChildren().add(image);
  }

  @Override
  public void update(long last, long now) {

  }

  @Override
  public void finish() {
    sprites.stop();
    image.setImage(ImageFactory.newInstance().getLooks(Looks.MENUPLAYER_DEFAULT));

    ft.stop();
  }
}
