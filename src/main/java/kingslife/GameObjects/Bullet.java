package kingslife.GameObjects;

import com.sun.javafx.geom.Vec2f;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Point3D;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import kingslife.GameObjects.collisions.bounds.Bounds;
import kingslife.gfx.ImageFactory;
import kingslife.gfx.Looks;

import java.util.function.Consumer;

public class Bullet extends GameObject {

  // Members
  private float _VX;
  private float _VY;

  private TranslateTransition _TT;
  //

  public static final float STANDARD_BULLET_WIDTH = 3 * 15; // 75
  public static final float STANDARD_BULLET_HEIGHT = 2 * 15; // 50

  private long started = System.currentTimeMillis();

  public boolean shotByPlayer;

  public Bullet(float x, float y, float mouseX, float mouseY, boolean shotByPlayer){
    // Size muss noch angepasst werden
    super(x ,y , 25 , 25);

    // Image hinzufügen
    ImageView bulletImage = new ImageView(ImageFactory.newInstance().getLooks(Looks.BULLET_LEFT_1));
    bulletImage.setFitWidth(STANDARD_BULLET_WIDTH);
    bulletImage.setFitHeight(STANDARD_BULLET_HEIGHT);

    myGroup.getChildren().add(bulletImage);

    // 10: Beschränkung der Geschw
    this._VX = (mouseX - x - STANDARD_BULLET_WIDTH/2) / 10;
    this._VY = (mouseY - y - STANDARD_BULLET_HEIGHT/2) / 10;
    double degrees = Math.toDegrees(Math.atan(_VY/_VX));
    if(_VX > 0) {
      degrees += 180;
    }
    bulletImage.setRotate(degrees);

    // nur mal als test hardcoded
    this._TT = new TranslateTransition(Duration.millis(20), myGroup);
    this._TT.setByX(_VX);
    this._TT.setByY(_VY);
    this._TT.setOnFinished(e-> {
      if((System.currentTimeMillis() - started)/1000 < 1) {
        this._TT.play();
      }else {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(500), myGroup);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.setOnFinished(event -> finish());
        fadeTransition.play();
      }

    });
    this._TT.play();

    this.shotByPlayer = shotByPlayer;
    createBounds();
  }

  public void createBounds() {
    bounds = new Bounds(this);
    bounds.setBox(new Vec2f(0, 0), STANDARD_BULLET_WIDTH, STANDARD_BULLET_HEIGHT);
  }

  @Override
  public void update(long last, long now) {

  }

  @Override
  public void finish() {
    myGroup.setVisible(false);
  }

  public float get_VX() {
    return _VX;
  }

  public void set_VX(float _VX) {
    this._VX = _VX;
  }

  public float get_VY() {
    return _VY;
  }

  public void set_VY(float _VY) {
    this._VY = _VY;
  }
}
