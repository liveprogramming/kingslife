package kingslife.GameObjects;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import kingslife.GameObjects.collisions.bounds.Bounds;

public abstract class GameObject {

  public SimpleFloatProperty x, y;
  public SimpleFloatProperty width, height;
  protected Group myGroup;

  public Bounds bounds;

  public GameObject() {
    myGroup = new Group();
    this.x = new SimpleFloatProperty();
    this.y = new SimpleFloatProperty();
    this.width = new SimpleFloatProperty();
    this.height = new SimpleFloatProperty();

    myGroup.layoutXProperty().bind(x);
    myGroup.layoutYProperty().bind(y);
  }

  public GameObject(float x, float y) {
    this();
    this.x.set(x);
    this.y.set(y);
  }

  public GameObject(float x, float y, float width, float height) {
    this(x,y);
    this.width.set(width);
    this.height.set(height);
  }

  public abstract void update(long last, long now);
  public abstract void finish();

  public Group getGroup()
  {
    return myGroup;
  }

}
