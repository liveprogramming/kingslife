package kingslife.GameObjects;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import kingslife.gfx.ImageFactory;
import kingslife.gfx.Looks;

public class Grass extends BackgroundObject {

  public static final int STANDARD_WIDTH = 120;
  public static final int STANDARD_HEIGHT = 120;

  private ImageView imageView;

  public Grass() {
    super(0,0,STANDARD_WIDTH,STANDARD_HEIGHT);
    
    initialize();
  }
  public Grass(int x, int y) {
    super(x,y,STANDARD_WIDTH, STANDARD_HEIGHT);

    initialize();
  }
  public Grass(int x, int y, int width, int height) {
    super(x,y,width,height);

    initialize();
  }

  private void initialize() {
    imageView = new ImageView(ImageFactory.newInstance().getLooks(Looks.GRASS_0));

    Image images[] = new Image[3];
    images[0] = ImageFactory.newInstance().getLooks(Looks.GRASS_NEW_1);
    images[1] = ImageFactory.newInstance().getLooks(Looks.GRASS_NEW_2);
    images[2] = ImageFactory.newInstance().getLooks(Looks.GRASS_NEW_3);

    // setup keyframe for animation
    KeyFrame keyFrames[] = new KeyFrame[3];
    keyFrames[0] = new KeyFrame(Duration.millis(500), e->imageView.setImage(images[0]));
    keyFrames[1] = new KeyFrame(Duration.millis(1000), e->imageView.setImage(images[1]));
    keyFrames[2] = new KeyFrame(Duration.millis(2000), e->imageView.setImage(images[2]));

    // wind-animation
    Timeline sprites = new Timeline();
    sprites.setCycleCount(Animation.INDEFINITE);
    sprites.getKeyFrames().setAll(keyFrames);
    sprites.play();

    imageView.setX(0);
    imageView.setY(0);
    imageView.fitWidthProperty().bind(width);
    imageView.fitHeightProperty().bind(height);

    myGroup.getChildren().addAll(imageView);
  }

  @Override
  public void update(long last, long now) {
  }

  @Override
  public void finish() {

  }
}
