package kingslife.GameObjects;

import com.sun.javafx.geom.Vec2f;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;
import kingslife.GameObjects.collisions.bounds.Bounds;
import kingslife.gfx.ImageFactory;
import kingslife.gfx.Looks;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Player extends GameObject {

  public float speed = 2;

  private ImageView playerImage;

  public static final float STANDARD_PLAYER_WIDTH = 70;
  public static final float STANDARD_PLAYER_HEIGHT = 70;

  // Multiplayer
  public StringProperty name = new SimpleStringProperty();
  public int nameUpdateSequence = Integer.MIN_VALUE;
  private int id = -1;
  public int sendSequence = Integer.MIN_VALUE, receiveSequence = Integer.MIN_VALUE;
  public BooleanProperty walking = new SimpleBooleanProperty(false);

  public Team team = Team.BLUE;
  private IntegerProperty health = new SimpleIntegerProperty(100);
  public SimpleFloatProperty rotation = new SimpleFloatProperty();

  private Label nameText;
  private Rectangle rectBar, rectHealth;

  private volatile boolean moving = false;
  private ConcurrentLinkedQueue<Vec2f> moveQueue = new ConcurrentLinkedQueue<>();

  public enum Team {
    BLUE((byte) 0), RED((byte) 1);

    public byte number;

    Team(byte number) {
      this.number = number;
    }

    public Team fromNumber(byte number) {
      for(Team t : values()) {
        if(t.number == number) return t;
      }
      return null;
    }
  }

  public Player() {
    super();
    initialize();
  }

  private void initialize() {
    width.set(STANDARD_PLAYER_WIDTH);
    height.set(STANDARD_PLAYER_HEIGHT);
    x.set(-100);
    y.set(-100);

    playerImage = new ImageView(ImageFactory.newInstance().getLooks(Looks.PLAYER_LEFT_1));
    playerImage.fitWidthProperty().bind(width);
    playerImage.fitHeightProperty().bind(height);

    nameText = new Label();
    nameText.textProperty().bind(name);
    nameText.layoutXProperty().bind(nameText.widthProperty().divide(2).multiply(-1).add(width.divide(2)));
    nameText.setLayoutY(-40);
    nameText.setTextFill(team == Team.RED ? Color.RED : Color.BLUE);
    nameText.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
    myGroup.getChildren().add(nameText);

    rectBar = new Rectangle(0, -15, (int) width.get(), 10);
    rectBar.setFill(Color.GREY);
    rectBar.setStroke(Color.SANDYBROWN);
    myGroup.getChildren().add(rectBar);

    rectHealth = new Rectangle(1, -14, (int) width.subtract(2).divide(100).multiply(health).floatValue(), 8);
    rectHealth.setFill(team == Team.RED ? Color.RED : Color.BLUE);
    rectHealth.setStroke(team == Team.RED ? Color.RED : Color.BLUE);
    myGroup.getChildren().add(rectHealth);

    myGroup.getChildren().add(playerImage);

    createBounds();
  }

  private void createBounds() {
    // TODO: Replace this with better bounds definition
    bounds = new Bounds(this);
    bounds.setBox(new Vec2f(0, 0), STANDARD_PLAYER_WIDTH, STANDARD_PLAYER_HEIGHT);
  }

  @Override
  public void update(long last, long now) {

  }

  @Override
  public void finish() {

  }

  public void move(float x, float y, float rotation, long ms) {
    if(!moving) {
      createTransition(x, y, ms);
    } else {
      moveQueue.offer(new Vec2f(x, y));
    }
    // TODO: Add rotation
  }

  private void createTransition(float x, float y, float ms) {
    moving = true;
    Timeline move = new Timeline(
            new KeyFrame(Duration.millis(0),
                    new KeyValue(this.x, this.x.get()),
                    new KeyValue(this.y, this.y.get())),
            new KeyFrame(Duration.millis(ms),
                    new KeyValue(this.x, x),
                    new KeyValue(this.y, y)));
    move.setOnFinished(e -> {
      moving = false;
      if(!moveQueue.isEmpty()) {
        Vec2f v = moveQueue.poll();
        createTransition(v.x, v.y, ms);
      }
    });
    move.play();
  }

  public void setName(String name) {
    Platform.runLater(() -> this.name.set(name));
  }

  public String getName() {
    return name.get();
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public int getHealth() {
    return this.health.intValue();
  }

  public void setHealth(int health) {
    this.health.set(health);
  }

  public void setTeam(Team team) {
    this.team = team;
    Color color = team == Team.RED ? Color.RED : Color.BLUE;
    nameText.setTextFill(color);
    rectHealth.setFill(color);
    rectHealth.setStroke(color);
  }
}
