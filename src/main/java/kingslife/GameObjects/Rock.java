package kingslife.GameObjects;

import com.sun.javafx.geom.Vec2f;
import javafx.scene.image.ImageView;
import kingslife.GameObjects.collisions.bounds.Bounds;
import kingslife.gfx.ImageFactory;
import kingslife.gfx.Looks;

public class Rock extends BackgroundObject {
    public Rock(int x, int y) {
        super(x,y,STANDARD_WIDTH,STANDARD_HEIGHT);
        imageView = new ImageView(ImageFactory.newInstance().getLooks(Looks.ROCK0));
        imageView.setX(0);
        imageView.setY(0);
        imageView.fitWidthProperty().bind(width);
        imageView.fitHeightProperty().bind(height);

        myGroup.getChildren().addAll(imageView);

        createBounds();
    }

    private void createBounds() {
        // TODO: Replace this with better bounds definition
        bounds = new Bounds(this);
        bounds.setBox(new Vec2f(0, 0), STANDARD_WIDTH, STANDARD_HEIGHT);
    }

    @Override
    public void update(long last, long now) {

    }

    @Override
    public void finish() {

    }

    public static final int STANDARD_WIDTH = 120;
    public static final int STANDARD_HEIGHT = 120;

    private ImageView imageView;

    public Rock(){
        this(0, 0);
    }
}
