package kingslife.GameObjects.collisions;

import kingslife.GameObjects.GameObject;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.BiConsumer;
import kingslife.GameObjects.Bullet;
import kingslife.GameObjects.Player;

public class PhysicsWorld {

  private BiConsumer<GameObject, GameObject> collisionHandler;
  private ConcurrentLinkedQueue<GameObject> objects = new ConcurrentLinkedQueue<>();

  public void addObject(GameObject object) {
    objects.add(object);
  }

  public void removeObject(GameObject object) {
    objects.remove(object);
  }

  public void update() {
    if(collisionHandler != null) {
      for(GameObject object : objects) {
        for(GameObject otherObject : objects) {
          if(object != otherObject && object.bounds != null && otherObject.bounds != null) {
            if (object instanceof Bullet && otherObject instanceof Player)
            if(object.bounds.isColliding(otherObject.bounds)) {
              collisionHandler.accept(object, otherObject);
            }
          }
        }
      }
    }
  }

  public void setCollisionHandler(BiConsumer<GameObject, GameObject> collisionHandler) {
    this.collisionHandler = collisionHandler;
  }

}
