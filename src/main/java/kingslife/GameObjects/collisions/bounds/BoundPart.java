package kingslife.GameObjects.collisions.bounds;

import com.sun.javafx.geom.Vec2f;

public interface BoundPart {

  void setTransform(Vec2f transform);
  boolean intersects(BoundPart part);
  int intersectionsToRight(BoundPart part);

}
