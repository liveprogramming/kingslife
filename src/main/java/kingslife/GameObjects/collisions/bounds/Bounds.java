package kingslife.GameObjects.collisions.bounds;

import com.sun.javafx.geom.Vec2f;
import kingslife.GameObjects.GameObject;

import java.util.Arrays;
import java.util.LinkedList;

public class Bounds {

  public Bounds(GameObject parent) {
    this.parent = parent;
  }

  private GameObject parent;
  private LinkedList<BoundPart> parts = new LinkedList<>();

  public void addParts(BoundPart... boundParts) {
    parts.addAll(Arrays.asList(boundParts));
  }

  public void setBox(Vec2f center, float width, float height) {
    parts.clear();
    parts.add(new BoundLine(new Vec2f(center.x - width/2, center.y - height/2), new Vec2f(center.x + width/2, center.y - height/2)));
    parts.add(new BoundLine(new Vec2f(center.x + width/2, center.y - height/2), new Vec2f(center.x + width/2, center.y + height/2)));
    parts.add(new BoundLine(new Vec2f(center.x + width/2, center.y + height/2), new Vec2f(center.x - width/2, center.y + height/2)));
    parts.add(new BoundLine(new Vec2f(center.x - width/2, center.y + height/2), new Vec2f(center.x - width/2, center.y - height/2)));
  }

  public boolean isColliding(Bounds bounds) {
    for(BoundPart part : parts) {
      part.setTransform(new Vec2f(parent.x.get(), parent.y.get()));
      for(BoundPart otherPart : bounds.parts) {
        otherPart.setTransform(new Vec2f(bounds.parent.x.get(), bounds.parent.y.get()));
        if(part.intersects(otherPart)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * This may return erroneous results if bounds is not closed geometry
   * @param bounds
   */
  public boolean isInside(Bounds bounds) {
    int intersections = 0;
    for(BoundPart part : parts) {
      part.setTransform(new Vec2f(parent.x.get(), parent.y.get()));
      for(BoundPart otherPart : bounds.parts) {
        otherPart.setTransform(new Vec2f(parent.x.get(), parent.y.get()));
        intersections += part.intersectionsToRight(otherPart);
      }
    }
    return intersections % 2 != 0;
  }

}
