package kingslife.Listener;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.HashSet;

public class KL {

  private static KL instance;

  public static final HashSet<KeyCode> keys;

  public final Pressed PRESSED;
  public final Released RELEASED;

  static {
    keys = new HashSet<KeyCode>();
  }

  private KL() {
    PRESSED = new Pressed();
    RELEASED = new Released();
  }

  public static KL getInstance() {
    if (instance == null) {
      synchronized (KL.class) {
        if (instance == null) instance = new KL();
      }
    }

    return instance;
  }

  private class Pressed implements EventHandler<KeyEvent> {
    @Override
    public void handle(KeyEvent event) {
      if (event.getCode() != KeyCode.ESCAPE) keys.add(event.getCode());
    }
  }

  private class Released implements EventHandler<KeyEvent> {
    @Override
    public void handle(KeyEvent event) {
      keys.remove(event.getCode());
    }
  }
}
