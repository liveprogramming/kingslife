package kingslife;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import kingslife.gfx.ImageFactory;
import kingslife.network.UpdateManager;
import kingslife.ui.PreGameMenu.PreGameMenu;
import kingslife.ui.SplashScreen;
import kingslife.ui.WindowManager;

import javax.swing.*;
import java.util.Arrays;

public class Main extends Application {

  private PreGameMenu preGameMenu;

  @Override
  public void start(Stage primaryStage) {
    WindowManager.getInstance().changeStage(primaryStage);

    Group g = new Group(); // temporary group
    Scene scene = new Scene(g);
    WindowManager.getInstance().changeScene(scene);

    preGameMenu = new PreGameMenu();
    preGameMenu.show();

    WindowManager.getInstance().showStage();
  }

  @Override
  public void init() throws Exception {
    super.init();

    UpdateManager.getInstance().start();

    Thread.sleep(1000);
  }

  public static void main(String[] args) {
    if(args.length > 0 && args[0].equalsIgnoreCase("deploy")) {
      try {
        Thread.sleep(10000);
      } catch (InterruptedException ex) {}
    }
    LauncherImpl.launchApplication(Main.class, SplashScreen.class, args);
  }
}
