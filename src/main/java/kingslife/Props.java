package kingslife;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Props {

  private Properties props;
  private File file = new File("kingslife.properties");
  private boolean changed;

  public Props() {
    props = new Properties();
    if(file.exists()) {
      try {
        props.load(new FileInputStream(file));
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  public String get(String key) {
    return props.getProperty(key);
  }

  public String get(String key, String def) {
    return props.getProperty(key, def);
  }

  public void set(String key, String value) {
    if(value == null) {
      props.remove(key);
    } else {
      props.setProperty(key, value);
    }
    changed = true;
  }

  public void close() {
    if(changed) {
      try {
        if(!file.exists()) file.createNewFile();
        props.store(new FileWriter(file), null);
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

}
