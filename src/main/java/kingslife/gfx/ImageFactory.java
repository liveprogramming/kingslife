package kingslife.gfx;

import java.util.HashMap;

import javafx.scene.image.Image;

/**
 * @author lukas
 */
public class ImageFactory {
  private static final ImageFactory imageFactory;

  private HashMap<Looks, Image> looks = new HashMap<Looks, Image>();

  static {
    imageFactory = new ImageFactory();
  }

  private ImageFactory() {
    for (Looks v : Looks.values()) {
      looks.put(v, new Image(getClass().getClassLoader().getResourceAsStream(v.path)));
    }
  }

  public static ImageFactory newInstance() {

    return imageFactory;
  }

  public Image getLooks(Looks look) {
    return looks.get(look);
  }

}