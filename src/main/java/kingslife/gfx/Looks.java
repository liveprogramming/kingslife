package kingslife.gfx;

public enum Looks {
  MENUPLAYER_LEFT_1("MenuPlayer/L_1.png"), MENUPLAYER_LEFT_2("MenuPlayer/L_2.png"),
  MENUPLAYER_RIGHT_1("MenuPlayer/R_1.png"), MENUPLAYER_RIGHT_2("MenuPlayer/R_2.png"),
  MENUPLAYER_DEFAULT("MenuPlayer/Default.png"),
  PLAYER_LEFT_1("player/L_1.png"),
  GRASS_0("grass/grass0.png"),
  GRASS_NEW_1("grass/grass_new_1.png"),GRASS_NEW_2("grass/grass_new_2.png"),GRASS_NEW_3("grass/grass_new_3.png"),
  ROCK0("rock/rock0.png"),

  BULLET_LEFT_1("bullet/bullet_left_1.png"),

  TREE0("trees/tree0.png")
  ;

  public String path;

  Looks(String path) {
    this.path = "gfx/" + path;
  }

}
