package kingslife.network;

import com.jona446.jhudp.Message;
import kingslife.GameObjects.Player;

public class MessageGenerator {

  private long timeDifference;

  public void setTimeDifference(long timeDifference) {
    this.timeDifference = timeDifference;
  }

  public Message generateConnect(boolean player) {
    return new Message(("C" + (player ? "0" : "1")).getBytes(), true);
  }

  public Message generateNameUpdate(Player player, String name) {
    return new Message(("U" + name + "," + ++player.nameUpdateSequence).getBytes(), true);
  }

  public Message generateSetTeam(boolean blue) {
    return new Message(("Y" + (blue ? "0" : "1")).getBytes(), true);
  }

  public Message generateStartGame() {
    return new Message("T".getBytes(), true);
  }

  public Message generateLocationUpdate(Player player) {
    int seq = player.sendSequence;
    if(player.sendSequence < Integer.MAX_VALUE) {
      player.sendSequence++;
    } else {
      player.sendSequence = Integer.MIN_VALUE;
    }
    return new Message(("X" + player.x.get() + "," + player.y.get() + "," + player.rotation.get() + "," +
            (player.walking.get() ? "1" : "0") + "," + seq).getBytes(), false);
  }

  public Message generateShotFired(float x, float y, float vecx, float vecy) {
    //long rawTime = System.currentTimeMillis();
    return new Message(("V" + x + "," + y + "," + vecx + "," + vecy).getBytes(), true);
  }

  public Message generateItemUsed(int x, int y, int itemid) {
    return new Message(("I" + x + "," + y + "," + itemid).getBytes(), true);
  }

  public Message generateObjectDamaged(int x, int y, int damage) {
    return new Message(("R" + x + "," + y + "," + damage).getBytes(), true);
  }

  public Message generateHealthUpdate(int health) {
    return new Message(("H" + health).getBytes(), true);
  }

  public Message generateGameEnd(boolean blueWins) {
    return new Message(("F" + (blueWins ? "0" : "1")).getBytes(), true);
  }

  public Message generateDisconnect() {
    return new Message("D".getBytes(), true);
  }

}
