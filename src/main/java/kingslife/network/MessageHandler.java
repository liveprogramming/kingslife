package kingslife.network;

import com.jona446.jhudp.Message;
import com.jona446.jhudp.MessageReceiver;
import kingslife.Data;
import kingslife.GameObjects.Bullet;
import kingslife.GameObjects.Player;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageHandler implements MessageReceiver {

  private Multiplayer multiplayer;

  public MessageHandler(Multiplayer multiplayer) {
    this.multiplayer = multiplayer;
  }

  @Override
  public void receive(Message message) {
    String raw = message.getDataAsString();
    if(raw.isEmpty()) return;
    String msg = raw.substring(1);
    msg = msg.replace("\n", "");

    char type = raw.charAt(0);
    switch(type) {
      case 'C': { // Connection acknowledgment
        multiplayer.setId(Integer.parseInt(msg));
        break;
      }
      case 'J': { // Player connected
        String[] parts = msg.split(",");
        for(String s : parts) {
          Player player = new Player();
          player.setId(Integer.parseInt(s));
          multiplayer.addPlayer(player);
          runCached(player.getId());
        }
        break;
      }
      case 'K': { // Player disconnected
        int id = Integer.parseInt(msg);
        Player player = multiplayer.getPlayer(id);
        if(player != null) {
          multiplayer.removePlayer(player);
        }
        break;
      }
      case 'S': { // Current game status
        multiplayer.inGame = msg.equals("1");
        if(multiplayer.isSpectator() && multiplayer.inGame && multiplayer.gameScreen == null) {
          multiplayer.startGame = true;
        }
        break;
      }
      case 'M': { // Set current time
        long dif = System.currentTimeMillis() - Long.parseLong(msg);
        multiplayer.messageGenerator.setTimeDifference(dif);
        multiplayer.timeDifference = dif;
        break;
      }
      case 'U': { // Set new name for player
        String[] parts = msg.split(",");
        int id = Integer.parseInt(parts[0]);
        int nameUpdateSequence = Integer.parseInt(parts[2]);
        if(id != multiplayer.id)  {
          Player player = multiplayer.getPlayer(id);
          if(player != null) {
            if(nameUpdateSequence >= player.nameUpdateSequence) {
              player.nameUpdateSequence = nameUpdateSequence;
              player.setName(parts[1]);
            }
          } else {
            cacheMessage(id, message);
          }
        }
        break;
      }
      case 'Y': { // Set team
        String[] parts = msg.split(",");
        int id = Integer.parseInt(parts[0]);
        if(id == multiplayer.id) {
          multiplayer.player.setTeam(Player.Team.BLUE.fromNumber(Byte.parseByte(parts[1])));
        } else {
          Player player = multiplayer.getPlayer(id);
          if(player != null) {
            player.setTeam(Player.Team.BLUE.fromNumber(Byte.parseByte(parts[1])));
          } else {
            cacheMessage(id, message);
          }
        }
        break;
      }
      case 'T': { // Start game
        if(multiplayer.gameScreen == null) multiplayer.startGame = true;
        multiplayer.inGame = true;
        break;
      }
      case 'X': { // Location update
        String parts[] = msg.split(",");
        int id = Integer.parseInt(parts[0]);
        if(id != multiplayer.id) {
          Player player = multiplayer.getPlayer(id);
          if(player != null) {
            int sequence = Integer.parseInt(parts[5]);
            if(sequence > player.receiveSequence ||
                    (sequence < player.receiveSequence && Math.abs(sequence - player.receiveSequence) >= Integer.MAX_VALUE)) {
              player.receiveSequence = sequence;
              player.walking.set(parts[4].equalsIgnoreCase("1"));
              player.move(Float.parseFloat(parts[1]), Float.parseFloat(parts[2]),
                      Float.parseFloat(parts[3]), Data.UPDATE_TIME);
            }
          }
        }
        break;
      }
      case 'V': { // Shot fired
        String[] parts = msg.split(",");
        Bullet bullet = new Bullet(Float.parseFloat(parts[1]), Float.parseFloat(parts[2]),
                Float.parseFloat(parts[3]), Float.parseFloat(parts[4]), false);
        multiplayer.shoot(bullet);
        break;
      }
      case 'I': { // TODO: Item used
        break;
      }
      case 'R': { // TODO: Object damaged
        break;
      }
      case 'H': { // Health updated
        String[] parts = msg.split(",");
        int id = Integer.parseInt(parts[0]);
        if(id != multiplayer.id) {
          Player player = multiplayer.getPlayer(id);
          if(player != null) {
            multiplayer.getPlayer(id).setHealth(Integer.parseInt(parts[1]));
          } else {
            cacheMessage(id, message);
          }
        }
        break;
      }
      case 'N': { // TODO: Death
        break;
      }
      case 'F': { // TODO: Game end
        multiplayer.inGame = false;
        break;
      }
      case 'D': { // Disconnect
        multiplayer.exitGame = true;
        multiplayer.disconnect();
        multiplayer.inGame = false;
        break;
      }
      default: {
        System.out.println("Unknown message type: " + type);
        break;
      }
    }
  }

  private void cacheMessage(long id, Message message) {
    if(!multiplayer.tmpMsgs.containsKey(id)) {
      multiplayer.tmpMsgs.put(id, new ConcurrentLinkedQueue<>());
    }
    multiplayer.tmpMsgs.get(id).add(message);
    multiplayer.lastTmpMsgUpdate = System.currentTimeMillis();
  }

  private void runCached(long id) {
    if(multiplayer.tmpMsgs.containsKey(id)) {
      Iterator<Message> it = multiplayer.tmpMsgs.get(id).iterator();
      while(it.hasNext()) {
        receive(it.next());
        it.remove();
      }
    }
  }

}
