package kingslife.network;

import com.jona446.jhudp.Config;
import com.jona446.jhudp.JUSocket;
import com.jona446.jhudp.Message;
import kingslife.Data;
import kingslife.GameObjects.Bullet;
import kingslife.GameObjects.Player;
import kingslife.Props;
import kingslife.ui.Game.GameScreen;
import kingslife.ui.PreGameMenu.PreGameMenu;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Multiplayer {


  private JUSocket jhudp;
  private TCPConnection tcp;
  private UDPHandler udp;

  private MessageHandler receiver;

  public final MessageGenerator messageGenerator = new MessageGenerator();

  boolean exitGame;
  boolean startGame;

  int id;
  boolean spectator;
  String name;
  long timeDifference;
  boolean inGame;

  GameScreen gameScreen;

  public Player player;
  private ConcurrentLinkedQueue<Player> players = new ConcurrentLinkedQueue<>();
  private ConcurrentLinkedQueue<Player> addQueue = new ConcurrentLinkedQueue<>();
  private ConcurrentLinkedQueue<Player> removeQueue = new ConcurrentLinkedQueue<>();
  private ConcurrentLinkedQueue<Bullet> bulletSpawnQueue = new ConcurrentLinkedQueue<Bullet>();

  // This is used to account for messages that arrive prior to its player's existence locally
  ConcurrentHashMap<Long, ConcurrentLinkedQueue<Message>> tmpMsgs = new ConcurrentHashMap<>();
  long lastTmpMsgUpdate;

  public void connect(boolean player) throws UnknownHostException {
    this.spectator = !player;
    Props props = new Props();
    InetAddress address = InetAddress.getByName(props.get("server", "127.0.0.1"));
    props.close();

    Config config = new Config();
    config.packet_size = 50;
    config.keepAlive = true;
    receiver = new MessageHandler(this);
    config.receiver = receiver;
    config.disconnectHandler = this::onDisconnect;
    if(Data.USE_JHUDP) {
      jhudp = new JUSocket(config, address, 8884);
    } else {
      tcp = new TCPConnection(receiver, address, 8884, config);
      tcp.start();

      udp = new UDPHandler(receiver, config.packet_size, address);
      udp.start();
    }
    send(messageGenerator.generateConnect(player));
  }

  public void update() {
    long time = System.currentTimeMillis();
    if(gameScreen != null) {
      Iterator<Player> it = addQueue.iterator();
      while(it.hasNext()) {
        gameScreen.addPlayer(it.next());
        it.remove();
      }
      it = removeQueue.iterator();
      while(it.hasNext()) {
        gameScreen.removePlayer(it.next());
        it.remove();
      }
      Iterator<Bullet> bit = bulletSpawnQueue.iterator();
      while(bit.hasNext()) {
        gameScreen.shoot(bit.next());
        bit.remove();
      }
    }
    if(!tmpMsgs.isEmpty() && (time - lastTmpMsgUpdate) > 1000) {
      tmpMsgs.clear();
      lastTmpMsgUpdate = time;
    }
    if(startGame) {
      GameScreen gameScreen = new GameScreen(this);
      gameScreen.show();
      startGame = false;
    } else if(exitGame) {
      PreGameMenu pgm = new PreGameMenu();
      pgm.show();
      exitGame = false;
      disconnect();
      inGame = false;
    }
  }

  public void send(Message message) {
    if(jhudp != null) {
      try {
        if(jhudp == null) return;
        jhudp.send(message);
      } catch (IOException ex) {}
    } else if(tcp != null) {
      if(message.getReliable()) {
        if(tcp == null) return;
        tcp.send(message);
      } else {
        if(udp == null) return;
        udp.send(message);
      }
    }
  }

  public void onDisconnect(InetAddress address, int port) {
    exitGame = true;
  }

  public void disconnect() {
    if(Data.USE_JHUDP) {
      jhudp.close();
    } else {
      tcp.close();
      udp.close();
    }
  }

  public void setId(int id) {
    this.id = id;
    if(player != null) {
      player.setId(id);
    }
  }

  public void setName(String name) {
    this.name = name;
    if(player != null) {
      player.setName(name);
    }
  }

  public void setPlayer(Player player) {
    this.player = player;
    player.setId(id);
    player.setName(name);
  }

  public void setGameScreen(GameScreen gameScreen) {
    this.gameScreen = gameScreen;
    addQueue.clear();
    addQueue.addAll(players);
  }

  public void addPlayer(Player player) {
    boolean contains = false;
    for(Player p : players) {
      if(p.getId() == player.getId()) {
        contains = true;
        break;
      }
    }
    if(!contains) {
      players.add(player);
      addQueue.add(player);
    }
  }

  public Player getPlayer(int id) {
    for(Player player : players) {
      if(player.getId() == id) return player;
    }
    return null;
  }

  public void removePlayer(Player player) {
    if(player != null) {
      players.remove(player);
      removeQueue.add(player);
    }
  }

  public ConcurrentLinkedQueue<Player> getPlayers() {
    return players;
  }

  public boolean isInGame() {
    return inGame;
  }

  public boolean isSpectator() {
    return spectator;
  }

  public void shoot(Bullet bullet) {
    bulletSpawnQueue.add(bullet);
  }

}
