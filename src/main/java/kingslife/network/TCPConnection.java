package kingslife.network;

import com.jona446.jhudp.Config;
import com.jona446.jhudp.Message;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class TCPConnection extends Thread {

  private InetAddress address;
  private int port;
  private Config config;
  private MessageHandler messageHandler;

  private Socket socket;

  private BufferedReader reader;
  private PrintWriter writer;

  public TCPConnection(MessageHandler messageHandler, InetAddress address, int port, Config config) {
    this.address = address;
    this.port = port;
    this.config = config;
    this.messageHandler = messageHandler;
    this.setDaemon(true);
  }

  @Override
  public void run() {
    try {
      socket = new Socket(address, port);
      socket.setTcpNoDelay(true);
      socket.setKeepAlive(true);

      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      writer = new PrintWriter(socket.getOutputStream());

      char[] buffer = new char[config.packet_size];
      while(!interrupted() && (reader.read(buffer)) != -1) {
        Message message = new Message(new String(buffer).getBytes(), socket.getInetAddress(), socket.getPort());
        messageHandler.receive(message);
      }
    } catch (SocketException ex) {
      // TODO: Handle?
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      config.disconnectHandler.accept(address, port);
    }
  }

  public void send(Message message) {
    byte[] data = message.getData();
    if(data.length < config.packet_size) {
      byte[] temp = new byte[config.packet_size];
      System.arraycopy(data, 0, temp, 0, data.length);
      data = temp;
    }
    if(writer != null) {
      writer.write(new String(data).toCharArray());
      writer.flush();
    }
  }

  public void close() {
    try {
      interrupt();
      if(socket != null) {
        socket.close();
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

}
