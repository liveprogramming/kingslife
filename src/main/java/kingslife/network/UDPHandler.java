package kingslife.network;

import com.jona446.jhudp.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;

public class UDPHandler extends Thread{

  public UDPHandler(MessageHandler messageHandler, int packet_size, InetAddress server) {
    this.messageHandler = messageHandler;
    this.packet_size = packet_size;
    this.server = server;
    this.setDaemon(true);
  }

  private MessageHandler messageHandler;
  private int packet_size;
  private DatagramSocket socket;

  private InetAddress server;

  private byte[] receiveBuffer;
  private DatagramPacket receivePacket;

  @Override
  public void run() {
    try {
      Random random = new Random();
      int localPort = random.nextInt((65535 - 49152) + 1) + 49152;
      socket = new DatagramSocket(localPort);
      receiveBuffer = new byte[packet_size];
      receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
      while(!interrupted()) {
        try {
          socket.receive(receivePacket);
          Message message = new Message(receivePacket.getData(), receivePacket.getAddress(), receivePacket.getPort(), false);
          messageHandler.receive(message);
        } catch (SocketException ex) {
          // TODO: Handle?
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void send(Message message) {
    try {
      byte[] data = message.getData();
      if(data.length < packet_size) {
        byte[] temp = new byte[packet_size];
        System.arraycopy(data, 0, temp, 0, data.length);
        data = temp;
      }
      socket.send(new DatagramPacket(data, packet_size, server, 8885));
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void close() {
    interrupt();
    socket.close();
  }

}
