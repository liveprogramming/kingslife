package kingslife.network;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class UpdateConnection extends Thread {

  private String server;

  private Socket socket;
  private long version;

  private String gamePath;

  UpdateConnection(String server, long version) {
    this.server = server;
    this.version = version;
    gamePath = new File(UpdateConnection.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
    if(!gamePath.endsWith(".jar")) {
      if(gamePath.charAt(gamePath.length() - 1) != '/' && gamePath.charAt(gamePath.length() - 1) != '\\') gamePath += '/';
      gamePath += "KingsLife.jar";
    }
  }

  @Override
  public void run() {
    try {
      socket = new Socket(server, 8886);
      socket.setKeepAlive(true);
      socket.setSoTimeout(0);

      InputStream in = socket.getInputStream();
      PrintWriter writer = new PrintWriter(socket.getOutputStream());

      writer.println(version + "");
      writer.flush();

      FileOutputStream out = null;

      boolean received = false;
      byte[] buffer = new byte[4096];
      int bytesRead;
      boolean expectVersion = true;
      while((bytesRead = in.read(buffer)) != -1) {
        int skip = 0;
        if(expectVersion) {
          out = new FileOutputStream(gamePath);
          received = true;
          for(byte b : buffer) {
            skip++;
            if(b == '\n') {
              byte[] temp = new byte[skip - 1];
              System.arraycopy(buffer, 0, temp, 0, temp.length);
              version = Long.parseLong(new String(temp, "utf-8"));
              break;
            }
          }
          expectVersion = false;
        }
        out.write(buffer, skip, bytesRead - skip);
      }
      if(received) {
        out.flush();
        out.close();
      }
      socket.close();

      if(received) UpdateManager.getInstance().receivedVersion(version, gamePath);
    } catch(SocketException ex) {

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void close() {
    try {
      interrupt();
      socket.close();
    } catch (Exception ex){}
  }

}
