package kingslife.network;

import kingslife.Props;

import java.io.IOException;

public class UpdateManager {

  private static final UpdateManager instance = new UpdateManager();
  private UpdateManager() {}
  public static UpdateManager getInstance() {
    return instance;
  }

  private UpdateConnection connection;
  private String server;

  public void start() {
    Props props = new Props();
    server = props.get("server");
    long version = Long.parseLong(props.get("version", "-1"));
    props.close();

    openConnection(version);
  }

  void receivedVersion(long version, String path) {
    Props props = new Props();
    props.set("version", version + "");
    props.close();

    try {
      System.out.println(path);
      Runtime.getRuntime().exec("java -jar " + path + " deploy");
      System.out.println("Updating game to version " + version);
      System.exit(0);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private void openConnection(long version) {
    connection = new UpdateConnection(server, version);
    connection.setDaemon(true);
    connection.start();
  }

  public void close() {
    if(connection != null) connection.close();
  }

}
