package kingslife.ui.Game;

import kingslife.GameObjects.Bullet;
import kingslife.GameObjects.GameObject;
import kingslife.GameObjects.Player;
import kingslife.GameObjects.collisions.PhysicsWorld;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CollisionListener {

  private PhysicsWorld world;

  private Player player;
  private ConcurrentLinkedQueue<Bullet> bullets = new ConcurrentLinkedQueue<Bullet>();

  public CollisionListener(PhysicsWorld world) {
    this.world = world;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }

  public void addBullet(Bullet bullet) {
    bullets.add(bullet);
  }

  public void update() {
    Iterator<Bullet> it = bullets.iterator();
    while(it.hasNext()) {
      Bullet bullet = it.next();
      if(!bullet.getGroup().isVisible()) {
        it.remove();
        world.removeObject(bullet);
      }
    }
  }

  public void onCollision(GameObject a, GameObject b) {
    System.out.println("c " + a + " " + b);
    if(containsInstance(Player.class, a, b) && containsInstance(Bullet.class, a, b)) {
      System.out.println("b");
      Player pl = (Player) getObject(Player.class, a, b)[0];
      Bullet bl = (Bullet) getObject(Bullet.class, a, b)[0];
      if(pl == player && !bl.shotByPlayer) {
        System.out.println("d");
        pl.setHealth(pl.getHealth() - 10);
        bl.finish();
      } else if(pl != player) {
        System.out.println("ua");
        bl.finish();
      }
    }
  }

  private boolean containsInstance(Class clazz, GameObject... objects) {
    for(GameObject object : objects) {
      if(clazz.isInstance(object)) return true;
    }
    return false;
  }

  private GameObject[] getObject(Class clazz, GameObject... objects) {
    LinkedList<GameObject> list = new LinkedList<>();
    for(GameObject object : objects) {
      if(clazz.isInstance(object)) list.add(object);
    }
    return list.toArray(new GameObject[list.size()]);
  }

}
