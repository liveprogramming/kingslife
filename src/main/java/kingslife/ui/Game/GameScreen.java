package kingslife.ui.Game;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import kingslife.Data;
import kingslife.GameObjects.Bullet;
import kingslife.GameObjects.Grass;
import kingslife.GameObjects.Player;
import kingslife.GameObjects.collisions.PhysicsWorld;
import kingslife.Listener.KL;
import kingslife.network.Multiplayer;
import kingslife.ui.Game.map.Map;
import kingslife.ui.GameScene;
import kingslife.ui.PreGameMenu.PreGameMenu;
import kingslife.ui.WindowManager;

import java.util.Timer;
import java.util.TimerTask;

public class GameScreen extends GameScene {

  private Scene myScene;
  private Group backgroundLayer;
  private Group playerLayer;

  private Map map;
  private Multiplayer multiplayer;
  private PhysicsWorld world;
  private CollisionListener collisionListener;

  private Player myPlayer;

  public Timer walkTimer = new Timer(true);

  public GameScreen(Multiplayer multiplayer) {
    super();

    this.multiplayer = multiplayer;
    multiplayer.setGameScreen(this);

    initialize();
  }

  private void initialize() {
    // background-Layer
    backgroundLayer = new Group();
    myGroup.getChildren().add(backgroundLayer);

    world = new PhysicsWorld();
    collisionListener = new CollisionListener(world);
    world.setCollisionHandler(collisionListener::onCollision);

    map = new Map(this, world);

    playerLayer = new Group();
    myGroup.getChildren().add(playerLayer);
    if (!multiplayer.isSpectator()) {
      // Player
      if (multiplayer.player == null) {
        myPlayer = new Player();
        multiplayer.setPlayer(myPlayer);
      } else {
        myPlayer = multiplayer.player;
      }
      world.addObject(myPlayer);
      collisionListener.setPlayer(myPlayer);
      
      if(myPlayer.team == Player.Team.RED) {
        myPlayer.x.set(map.getWidth() - Data.SCREEN_WIDTH / 2 - myPlayer.width.get() / 2);
      } else {
        myPlayer.x.set(Data.SCREEN_WIDTH / 2 - myPlayer.width.get() / 2);
      }
      myPlayer.y.set(map.getHeight()/2 - Data.SCREEN_HEIGHT / 2 - myPlayer.height.get() / 2);

      myGroup.setLayoutX(-myPlayer.x.get() + Data.SCREEN_WIDTH/2 - myPlayer.width.get()/2);
      myGroup.setLayoutY(-myPlayer.y.get() + Data.SCREEN_HEIGHT/2 - myPlayer.height.get()/2);

      playerLayer.getChildren().add(myPlayer.getGroup());
    }

    if (!multiplayer.isSpectator()) {
      // MouseListener
      myGroup.setOnMouseClicked(e -> {
        shoot(myPlayer.x.get(), myPlayer.y.get(), (float) e.getX(), (float) e.getY(), true);
        multiplayer.send(multiplayer.messageGenerator.generateShotFired(
                myPlayer.x.get(), myPlayer.y.get(), (float) e.getX(), (float) e.getY()));
      });
    }

    walkTimer.schedule(new TimerTask() {
      private int timesNotMoved = 0;

      private float lastX;
      private float lastY;

      @Override
      public void run() {
        if(lastX == myPlayer.x.get() && lastY == myPlayer.y.get()) {
          if (timesNotMoved < 60) {
            timesNotMoved++;
          } else {
            // KICK
            Platform.runLater(() -> {
              PreGameMenu pgm = new PreGameMenu();
              pgm.show();
              multiplayer.send(multiplayer.messageGenerator.generateDisconnect());
            });
            this.cancel();
          }
        } else {
          timesNotMoved = 0;
          lastX = myPlayer.x.get();
          lastY = myPlayer.y.get();
        }
      }
    }, 0, 1_000);
  }

  @Override
  public void show() {
    WindowManager.getInstance().switchOffGrassBackground();
    WindowManager.getInstance().changeGameGroup(this, true, true);

    if (multiplayer.isSpectator()) {
      int height = map.getHeight();
      int width = (int) ((float) height / 9f * 16f);
      myGroup.setLayoutX(width / 2 - (Grass.STANDARD_WIDTH / 2));
      if (width < map.getWidth()) {
        width = map.getWidth();
        height = (int) ((float) width / 16f * 9f);
        myGroup.setLayoutX(0);
        myGroup.setLayoutY(height / 2 - (Grass.STANDARD_HEIGHT / 2));
      }
      WindowManager.getInstance().updateViewport(width == 0 ? Data.SCREEN_WIDTH : width, height == 0 ? Data.SCREEN_HEIGHT : height);
    }
  }

  @Override
  public void update(long last, long now) {
    multiplayer.update();

    if (multiplayer.isSpectator()) {
      // TODO: Automatic camera handling?
    } else {
      float tmpX, tmpY;
      tmpX = tmpY = 0;
      if (KL.keys.contains(KeyCode.A)) tmpX--;
      if (KL.keys.contains(KeyCode.D)) tmpX++;
      if (KL.keys.contains(KeyCode.W)) tmpY++;
      if (KL.keys.contains(KeyCode.S)) tmpY--;

      float speed = myPlayer.speed * (now - last) / 10_000_000;

      // refresh position every second (for new clients)
      if ((now - lastSend) / 1_000_000 > 1000) {
        multiplayer.send(multiplayer.messageGenerator.generateLocationUpdate(myPlayer));

        lastSend = now;
      }

      if (tmpX != 0 || tmpY != 0) move(tmpX * speed, tmpY * speed, now);
    }
    map.update();
    collisionListener.update();
    world.update();
  }

  private long lastSend = System.currentTimeMillis();

  private void move(float x, float y, long now) {

    int mapHeight = map.getHeight();
    int mapWidth = map.getWidth();

    myPlayer.x.set(myPlayer.x.get() + x);
    myPlayer.y.set(myPlayer.y.get() - y);

    if ((myPlayer.x.get() + myPlayer.width.get() / 2) > Data.SCREEN_WIDTH / 2) {
      if ((myPlayer.x.get() + myPlayer.width.get() / 2) < (mapWidth - Data.SCREEN_WIDTH / 2)) {
        myGroup.setLayoutX(myGroup.getLayoutX() - x);
      } else {
        myGroup.setLayoutX(-(mapWidth - Data.SCREEN_WIDTH));
      }
    } else {
      myGroup.setLayoutX(0);
    }
    if ((myPlayer.y.get() + myPlayer.height.get() / 2) > Data.SCREEN_HEIGHT / 2) {
      if ((myPlayer.y.get() + myPlayer.height.get() / 2) < (mapHeight - Data.SCREEN_HEIGHT / 2)) {
        myGroup.setLayoutY(myGroup.getLayoutY() + y);
      } else {
        myGroup.setLayoutY(-(mapHeight - Data.SCREEN_HEIGHT));
      }
    } else {
      myGroup.setLayoutY(0);
    }

    // update over network
    if ((now - lastSend) / 1_000_000 > Data.UPDATE_TIME) {
      multiplayer.send(multiplayer.messageGenerator.generateLocationUpdate(myPlayer));

      lastSend = now;
    }
  }

  public void addPlayer(Player player) {
    playerLayer.getChildren().add(player.getGroup());
    world.addObject(player);
  }

  public void removePlayer(Player player) {
    playerLayer.getChildren().remove(player.getGroup());
    world.removeObject(player);
  }

  public void addLayer(boolean background, Group layer) {
    if (background) {
      int playerLayerPosition = 1, index = 0;
      for (Node n : myGroup.getChildren()) {
        if (n == playerLayer) {
          playerLayerPosition = index;
          break;
        }
        index++;
      }
      myGroup.getChildren().add(playerLayerPosition, layer);
    } else {
      myGroup.getChildren().add(layer);
    }
  }

  public void shoot(float x, float y, float vecx, float vecy, boolean shotByPlayer) {
    Bullet myBullet = new Bullet(x, y, vecx, vecy, shotByPlayer);
    world.addObject(myBullet);
    collisionListener.addBullet(myBullet);
    myGroup.getChildren().add(myBullet.getGroup());
  }

  public void shoot(Bullet bullet) {
    myGroup.getChildren().add(bullet.getGroup());
  }

}