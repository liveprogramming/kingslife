package kingslife.ui.Game.map;

import javafx.scene.Group;
import kingslife.GameObjects.GameObject;
import kingslife.GameObjects.Grass;
import kingslife.GameObjects.collisions.PhysicsWorld;
import kingslife.ui.Game.GameScreen;

import java.util.List;

public class Map {

  private GameScreen gameScreen;
  private List<MapLayer> layers;

  private int width, height;

  private PhysicsWorld world;

  public Map(GameScreen gameScreen, PhysicsWorld world) {
    this.gameScreen = gameScreen;
    this.world = world;
    load();
  }

  private void load() {
    this.layers = new MapLoader().load();
    for(MapLayer layer : layers) {
      Group g = new Group();
      for(int y = 0; y < layer.gameObjects.length; y++) {
        GameObject[] row = layer.gameObjects[y];
        for(int x = 0; x < row.length; x++) {
          if(row[x] != null) {
            g.getChildren().add(row[x].getGroup());
            world.addObject(row[x]);
          }
        }
      }
      layer.fxlayer = g;
      gameScreen.addLayer(layer.background, layer.fxlayer);
    }
    if(layers.size() > 0) {
      // TODO: Check whether width and height should be flipped
      height = layers.get(0).gameObjects.length * Grass.STANDARD_HEIGHT;
      width = layers.get(0).gameObjects[0].length * Grass.STANDARD_WIDTH;
    }
  }

  public void update() {

  }

  // TODO: Check whether x and y should be flipped
  public GameObject getObjectAt(int x, int y, String layer) {
    for(MapLayer l : layers) {
      if(l.name.equalsIgnoreCase(layer)) {
        return l.gameObjects[y][x];
      }
    }
    return null;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

}
