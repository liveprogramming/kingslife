package kingslife.ui.Game.map;

import javafx.scene.Group;
import kingslife.GameObjects.GameObject;

public class MapLayer {

  public String name;
  public GameObject[/*row (y)*/][/*column (x)*/] gameObjects;
  public boolean background;

  public Group fxlayer;

}
