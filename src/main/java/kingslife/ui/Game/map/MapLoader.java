package kingslife.ui.Game.map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import kingslife.Data;
import kingslife.GameObjects.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MapLoader {

    public List<MapLayer> load() {
        Gson gson = new Gson();

        JsonArray root = gson.fromJson(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(Data.MAP_PATH)), JsonArray.class);

        ArrayList<MapLayer> tmpList = new ArrayList<>();

        for (JsonElement t : root) {
            JsonArray tmp = t.getAsJsonArray();
            GameObject[][] gameObject = new GameObject[tmp.get(0).getAsJsonArray().size()][tmp.size()];
            for (int x = 0; x < tmp.size(); x++) {
                JsonArray tmp2 = tmp.get(x).getAsJsonArray();

                for (int y = 0; y < tmp2.size(); y++) {
                    int id = tmp2.get(y).getAsInt();
                    GameObject a = null;
                    switch (id) {
                        case 0:
                            a = new Grass(x * Grass.STANDARD_WIDTH, y * Grass.STANDARD_HEIGHT);
                            break;
                        case 1:
                            a = new Rock(x * Rock.STANDARD_WIDTH, y * Rock.STANDARD_HEIGHT);
                            break;
                    }
                    gameObject[y][x] = a;
                }
            }

            MapLayer l = new MapLayer();
            l.background = true;
            l.gameObjects = gameObject;
            tmpList.add(l);
        }

        return tmpList;
    }

}