package kingslife.ui;

import javafx.scene.Group;

public abstract class GameScene {

  protected Group myGroup;

  public GameScene() {
    myGroup = new Group();
  }

  public abstract void update(long last, long now);

  public void show() {
    WindowManager.getInstance().changeGameGroup(this, true, false);
  }

  public Group getMyGroup() {
    return myGroup;
  }
}
