package kingslife.ui.LobbyMenu;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Duration;
import kingslife.Data;
import kingslife.GameObjects.Player;
import kingslife.ui.Game.GameScreen;
import kingslife.network.Multiplayer;
import kingslife.ui.GameScene;
import kingslife.ui.WindowManager;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LobbyMenu extends GameScene {

  private Label lbLobbyMenu, lbGo, lbNameWarning;
  private TextField tfName;
  private Rectangle recGo;
  private Circle cirSwitchTeam;
  private DropShadow dsSwitchTeam, dsGo, dsNameWarning;
  private Line lBase, lTop, lBottom;
  private boolean teamChosen = false;
  private Transition arrowAnimation = new Transition() {

    {
      setCycleDuration(Duration.millis(500));
    }

    @Override
    protected void interpolate(double frac) {
      lTop.setEndX(-15 + (frac * 30));
      lTop.setTranslateX(lBase.getEndX() / 2 + ((lBase.getStartX() / 2 - lBase.getEndX() / 2) * frac));
      lBottom.setEndX(-15 + (frac * 30));
      lBottom.setTranslateX(lBase.getEndX() / 2 + ((lBase.getStartX() / 2 - lBase.getEndX() / 2) * frac));
    }
  };

  public List<Player> plTeamRed = new ArrayList<>();
  public List<Player> plTeamBlue = new ArrayList<>();

  private List<Label> lbTeamRed = new ArrayList<>();
  private List<Label> lbTeamBlue = new ArrayList<>();

  private Multiplayer multiplayer;
  private boolean player;


  public LobbyMenu(boolean player) {
    super();
    this.player = player;
    initialize();
  }

  private void initialize() {
    multiplayer = new Multiplayer();

    // create Lobby label
    lbLobbyMenu = new Label("Lobby");
    lbLobbyMenu.setFont(Font.font("Verdana", 30));
    lbLobbyMenu.setTextFill(Color.WHITE);
    lbLobbyMenu.setLayoutY(50);
    lbLobbyMenu.layoutXProperty().bind(lbLobbyMenu.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));

    if (player) {
      // Lobby for player
      createPlayerElements();
    }

    // create player labels for team blue
    lbTeamBlue = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
      Label labelBlue = new Label();
      labelBlue.setFont(Font.font("Verdana", 30));
      labelBlue.setTextFill(Color.DARKBLUE);
      labelBlue.setTranslateY(i * (labelBlue.getFont().getSize() + 5));
      labelBlue.translateXProperty().bind(labelBlue.widthProperty().divide(-2));
      lbTeamBlue.add(labelBlue);
    }
    // stackpane where team blue labels are collected
    StackPane spTeamBlue = new StackPane();
    spTeamBlue.setPickOnBounds(false);
    spTeamBlue.layoutYProperty().bind(spTeamBlue.heightProperty().multiply(-2).add(Data.SCREEN_HEIGHT / 2).subtract(50));
    spTeamBlue.layoutXProperty().bind(spTeamBlue.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2).subtract(100));
    spTeamBlue.getChildren().addAll(lbTeamBlue);

    // create player labels for team red
    lbTeamRed = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
      Label labelRed = new Label();
      labelRed.setFont(Font.font("Verdana", 30));
      labelRed.setTextFill(Color.DARKRED);
      labelRed.setTranslateY(i * (labelRed.getFont().getSize() + 5));
      lbTeamRed.add(labelRed);
    }
    // stackpane where team red labels are collected
    StackPane spTeamRed = new StackPane();
    spTeamRed.setPickOnBounds(false);
    spTeamRed.setAlignment(Pos.CENTER_LEFT);
    spTeamRed.layoutYProperty().bind(spTeamRed.heightProperty().multiply(-2).add(Data.SCREEN_HEIGHT / 2).subtract(50));
    spTeamRed.setLayoutX(Data.SCREEN_WIDTH / 2 + 100);
    spTeamRed.getChildren().addAll(lbTeamRed);

    myGroup.getChildren().addAll(lbLobbyMenu, spTeamBlue, spTeamRed);

    try {
      multiplayer.connect(player);
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    if (player) {
      Player player = new Player();
      multiplayer.setPlayer(player);
    }
  }

  private void createPlayerElements() {
    // create textfield for player name
    tfName = new TextField("");
    tfName.setLayoutY(100);
    tfName.layoutXProperty().bind(tfName.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));
    tfName.setOnKeyReleased(e -> {
      if (tfName.getText().length() > 15) {
        tfName.setText(tfName.getText().substring(0, 15));
        tfName.positionCaret(15);
      } else {
        multiplayer.player.setName(tfName.getText());
        multiplayer.send(multiplayer.messageGenerator.generateNameUpdate(multiplayer.player, tfName.getText()));
      }
    });

    // create dropshadow for player name warning
    dsNameWarning = new DropShadow();
    dsNameWarning.setRadius(1.0);
    dsNameWarning.setOffsetX(0.0);
    dsNameWarning.setOffsetY(0.0);
    dsNameWarning.setColor(Color.YELLOW);

    // create player name warning for when the textfield is empty
    lbNameWarning = new Label("Enter a name!");
    lbNameWarning.setFont(Font.font("Verdana", 15));
    lbNameWarning.setTextFill(Color.DARKRED);
    lbNameWarning.setEffect(dsNameWarning);
    lbNameWarning.visibleProperty().bind(tfName.textProperty().isEqualTo(""));
    lbNameWarning.layoutYProperty().bind(tfName.layoutYProperty()
            .add(tfName.heightProperty().divide(2))
            .subtract(lbNameWarning.heightProperty().divide(2)));
    lbNameWarning.layoutXProperty().bind(tfName.layoutXProperty().add(tfName.widthProperty().add(5)));

    // DropShadow for Team Switch button
    dsSwitchTeam = new DropShadow();
    dsSwitchTeam.setRadius(0.0);
    dsSwitchTeam.setOffsetX(0.0);
    dsSwitchTeam.setOffsetY(0.0);
    dsSwitchTeam.setColor(Color.YELLOW);

    // create Team Switch button
    cirSwitchTeam = new Circle(25);
    //cirSwitchTeam.setFill(Paint.valueOf("228B22"));
    cirSwitchTeam.setFill(Color.TRANSPARENT);
    cirSwitchTeam.setEffect(dsSwitchTeam);
    StackPane spSwitchTeam = new StackPane();
    spSwitchTeam.setLayoutY(200);
    spSwitchTeam.layoutXProperty().bind(spSwitchTeam.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));
    spSwitchTeam.setOnMouseClicked((e) -> {
      if (multiplayer.player.team == Player.Team.RED) {
        //currTeam = Player.Team.BLUE;
        if(!(plTeamBlue.size() >= 5)) {
          arrowAnimation.stop();
          arrowAnimation.setAutoReverse(true);
          arrowAnimation.setCycleCount(2);
          arrowAnimation.playFrom(arrowAnimation.getCycleDuration());
          multiplayer.send(multiplayer.messageGenerator.generateSetTeam(true));
        }
      } else {
        //currTeam = Player.Team.RED;
        if(!(plTeamRed.size() >= 5)) {
          arrowAnimation.stop();
          arrowAnimation.setAutoReverse(false);
          arrowAnimation.setCycleCount(1);
          arrowAnimation.play();
          multiplayer.send(multiplayer.messageGenerator.generateSetTeam(false));
        }
      }
    });
    spSwitchTeam.setOnMouseEntered(e -> {
      dsSwitchTeam.setRadius(5);
    });
    spSwitchTeam.setOnMouseExited(e -> {
      dsSwitchTeam.setRadius(0);
    });

    lBase = new Line(-15, 0, 15, 0);
    lBase.setStroke(Color.WHITE);
    lBase.setLayoutX(spSwitchTeam.getLayoutX());
    lBase.setLayoutY(spSwitchTeam.getLayoutY());
    lBase.setStrokeWidth(2.0f);
    lBase.setVisible(false);
    lTop = new Line(0, 0, -15, -15);
    lTop.setStroke(Color.WHITE);
    lTop.setStrokeWidth(2.0f);
    lTop.setTranslateX(lBase.getEndX() / 2);
    lTop.setTranslateY(-7.5);
    lTop.setVisible(false);
    lBottom = new Line(0, 0, -15, 15);
    lBottom.setStroke(Color.WHITE);
    lBottom.setStrokeWidth(2.0f);
    lBottom.setTranslateX(lBase.getEndX() / 2);
    lBottom.setTranslateY(7.5);
    lBottom.setVisible(false);

    spSwitchTeam.getChildren().addAll(cirSwitchTeam, lBase, lTop, lBottom);

    // DropShadow for Go button
    dsGo = new DropShadow();
    dsGo.setRadius(0.0);
    dsGo.setOffsetX(0.0);
    dsGo.setOffsetY(0.0);
    dsGo.setColor(Color.YELLOW);

    // create Go button
    recGo = new Rectangle(80, 50);
    recGo.setArcHeight(15);
    recGo.setArcWidth(15);
    //recGo.setFill(Paint.valueOf("228B22"));
    recGo.setFill(Color.TRANSPARENT);
    recGo.setEffect(dsGo);
    StackPane spGo = new StackPane();
    spGo.layoutYProperty().bind(spGo.heightProperty().multiply(-1).add(Data.SCREEN_HEIGHT).subtract(20));
    spGo.layoutXProperty().bind(spGo.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));
    spGo.setOnMouseClicked(e -> {
      if (!tfName.getText().isEmpty()) {
        GameScreen game = new GameScreen(multiplayer);
        game.show();
        if (!multiplayer.isInGame()) {
          multiplayer.send(multiplayer.messageGenerator.generateStartGame());
        }
      }
    });
    spGo.setOnMouseEntered(e -> {
      dsGo.setRadius(5);
    });
    spGo.setOnMouseExited(e -> {
      dsGo.setRadius(0);
    });
    lbGo = new Label("Go!");
    lbGo.setFont(Font.font("Verdana", 30));
    lbGo.setTextFill(Color.WHITE);
    spGo.getChildren().addAll(recGo, lbGo);

    myGroup.getChildren().addAll(tfName, lbNameWarning, spSwitchTeam, spGo);
  }

  @Override
  public void show() {
    WindowManager.getInstance().changeGameGroupWithLeftAnimation(this);
  }

  @Override
  public void update(long last, long now) {
    multiplayer.update();

    // Local player
    if (multiplayer.player != null) {
      if (!teamChosen) {
        lBase.setVisible(true);
        lTop.setVisible(true);
        lBottom.setVisible(true);
        if (multiplayer.player.team == Player.Team.BLUE) {
          lTop.setEndX(-15);
          lTop.setTranslateX(lBase.getEndX() / 2);
          lBottom.setEndX(-15);
          lBottom.setTranslateX(lBase.getEndX() / 2);
        } else {
          lTop.setEndX(15);
          lTop.setTranslateX(lBase.getStartX() / 2);
          lBottom.setEndX(15);
          lBottom.setTranslateX(lBase.getStartX() / 2);
        }
        teamChosen = true;
      }
      if (multiplayer.player.team == Player.Team.BLUE) {
        if (!lbTeamBlue.get(0).textProperty().isBound()) {
          lbTeamBlue.get(0).textProperty().bind(multiplayer.player.name);
        }
        if (lbTeamRed.get(0).textProperty().isBound()) {
          lbTeamRed.get(0).textProperty().unbind();
          lbTeamRed.get(0).setText("");
        }
      } else {
        if (!lbTeamRed.get(0).textProperty().isBound()) {
          lbTeamRed.get(0).textProperty().bind(multiplayer.player.name);
        }
        if (lbTeamBlue.get(0).textProperty().isBound()) {
          lbTeamBlue.get(0).textProperty().unbind();
          lbTeamBlue.get(0).setText("");
        }
      }
    }
    // Add players
    int blueIndex = 1, redIndex = 1;
    Iterator<Player> it = multiplayer.getPlayers().iterator();
    while (it.hasNext()) {
      Player player = it.next();
      if (player.team == Player.Team.BLUE && !plTeamBlue.stream().anyMatch(p -> p.getId() == player.getId())) {
        plTeamBlue.add(player);
        lbTeamBlue.get(blueIndex).textProperty().bind(player.name);
        blueIndex++;
      }
      if (player.team == Player.Team.RED && !plTeamRed.stream().anyMatch(p -> p.getId() == player.getId())) {
        plTeamRed.add(player);
        lbTeamRed.get(redIndex).textProperty().bind(player.name);
        redIndex++;
      }
    }
    // Remove players
    removePlayers();
  }

  private void removePlayers() {
    Iterator<Player> it = plTeamBlue.iterator();
    rm(Player.Team.BLUE, it, lbTeamBlue);
    it = plTeamRed.iterator();
    rm(Player.Team.RED, it, lbTeamRed);
  }

  private void rm(Player.Team team, Iterator<Player> it, List<Label> labels) {
    while (it.hasNext()) {
      Player player = it.next();
      if (!multiplayer.getPlayers().stream().anyMatch(p -> p.getId() == player.getId() && p.team == team)) {
        labels.forEach(lbl -> {
          if (lbl != null && player != null && player.getName() != null) {
            if (lbl.getText().equals(player.getName())) {
              lbl.textProperty().unbind();
              lbl.setText("");
            }
          }
        });
        it.remove();
      }
    }
  }

}
