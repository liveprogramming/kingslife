package kingslife.ui.PreGameMenu;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import kingslife.Data;
import kingslife.GameObjects.BackgroundPlayer;
import kingslife.Props;
import kingslife.ui.GameScene;
import kingslife.ui.LobbyMenu.LobbyMenu;
import kingslife.ui.WindowManager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PreGameMenu extends GameScene {

  private Label lbStartGame;
  private Label lbCredits;

  private DropShadow dropShadow, dropShadow2;

  private Props props;
  private boolean player;

  private ArrayList<BackgroundPlayer> backgroundPlayers;

  public PreGameMenu() {
    super();
    initialize();
  }

  private void initialize() {
    WindowManager.getInstance().switchOnGrassBackground();

    backgroundPlayers = new ArrayList<>();

    props = new Props();
    player = !props.get("spectate", "").equalsIgnoreCase("true");

    if(player) {
      createButtons();
    } else {
      new Timer().schedule(new TimerTask() {
        @Override
        public void run() {
          Platform.runLater(() -> {
            backgroundPlayers.forEach(x -> x.finish());
            LobbyMenu lobbyMenu = new LobbyMenu(player);
            lobbyMenu.show();
          });
        }
      }, 1000);
    }
  }

  private void createButtons() {
    // Drop Shadow for labels
    dropShadow = new DropShadow();
    dropShadow.setRadius(0.0);
    dropShadow.setOffsetX(0.0);
    dropShadow.setOffsetY(0.0);
    dropShadow.setColor(Color.YELLOW);

    lbStartGame = new Label("Start Game");
    lbStartGame.setFont(Font.font("Verdana", 30));
    lbStartGame.setTextFill(Color.WHITE);
    lbStartGame.setEffect(dropShadow);
    lbStartGame.layoutXProperty().bind(lbStartGame.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));
    lbStartGame.setLayoutY(200);
    lbStartGame.setOnMouseClicked(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        backgroundPlayers.forEach(x -> x.finish());
        LobbyMenu lobbyMenu = new LobbyMenu(player);
        lobbyMenu.show();
      }
    });
    lbStartGame.setOnMouseEntered(e -> {
      dropShadow.setRadius(5);
    });
    lbStartGame.setOnMouseExited(e -> {
      dropShadow.setRadius(0);
    });

    dropShadow2 = new DropShadow();
    dropShadow2.setRadius(0.0);
    dropShadow2.setOffsetX(0.0);
    dropShadow2.setOffsetY(0.0);
    dropShadow2.setColor(Color.YELLOW);

    lbCredits = new Label("Credits");
    lbCredits.setFont(Font.font("Verdana", 30));
    lbCredits.setEffect(dropShadow2);
    lbCredits.setTextFill(Color.WHITE);
    lbCredits.layoutXProperty().bind(lbCredits.widthProperty().divide(-2).add(Data.SCREEN_WIDTH / 2));
    lbCredits.setLayoutY(300);
    lbCredits.setOnMouseEntered(e -> {
      dropShadow2.setRadius(5);
    });
    lbCredits.setOnMouseExited(e -> {
      dropShadow2.setRadius(0);
    });

    myGroup.getChildren().addAll(lbStartGame, lbCredits);
  }

  @Override
  public void show() {
    WindowManager.getInstance().changeGameGroup(this, true, true);
    WindowManager.getInstance().switchOnGrassBackground();
  }

  private long lastFrame = 0;

  @Override
  public void update(long last, long now) {
    //KL.keys.iterator().forEachRemaining(System.out::println);

    // generate random player in the background
    if ((now - lastFrame) / 1_000_000_000 > 1) {
      lastFrame = now;

      // setup tmp-variables
      BackgroundPlayer newBackgroundPlayer = new BackgroundPlayer();
      backgroundPlayers.add(newBackgroundPlayer);
      myGroup.getChildren().add(newBackgroundPlayer.getGroup());
    }
  }
}
