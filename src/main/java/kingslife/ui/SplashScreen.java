package kingslife.ui;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kingslife.network.UpdateManager;

public class SplashScreen extends Preloader{

  private Stage myStage;

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.myStage = primaryStage;

    BorderPane parent = new BorderPane();

    ProgressIndicator loadingIndicator = new ProgressIndicator();
    loadingIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
    loadingIndicator.setMaxWidth(100);
    loadingIndicator.setMaxHeight(100);

    parent.setCenter(loadingIndicator);

    Scene scene = new Scene(parent, 400,400);

    primaryStage.setScene(scene);
    primaryStage.initStyle(StageStyle.UNDECORATED);
    primaryStage.show();

  }

  @Override
  public void handleStateChangeNotification(StateChangeNotification stateChangeNotification) {
    if (stateChangeNotification.getType() == StateChangeNotification.Type.BEFORE_START) {
      myStage.hide();
    }
  }

}
