package kingslife.ui;

import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import kingslife.Data;
import kingslife.GameObjects.Grass;
import kingslife.Listener.KL;

public class WindowManager {

  private static WindowManager instance;

  private Stage currentStage;
  private Scene currentScene;
  private Group rootGroup;
  private Group currentGroup;

  private static final Duration STANDARD_SWITCH_DURATION = Duration.seconds(2);

  // Background
  private Group background;

  private AnimationTimer currentUpdateTimer;
  private long lastAnimation = System.currentTimeMillis();

  private WindowManager() {
    rootGroup = new Group();
    // scale
    rootGroup.getTransforms().addAll(Data.scale);

    // Background
    background = new Group();
    rootGroup.getChildren().add(background);
  }

  public static WindowManager getInstance() {
    if (instance == null) {
      synchronized (WindowManager.class) {
        if (instance == null) instance = new WindowManager();
      }
    }

    return instance;
  }

  public void changeStage(Stage nextStage) {
    if (currentStage != null) currentStage.close();

    currentStage = nextStage;

    currentStage.setWidth(Data.SCREEN_WIDTH);
    currentStage.setHeight(Data.SCREEN_HEIGHT);
  }

  public void showStage() {
    currentStage.show();
  }

  public void changeScene(Scene nextScene) {
    currentStage.setScene(nextScene);

    currentStage.setFullScreen(Data.fullscreen);

    currentScene = nextScene;

    // setup keylistener
    currentScene.setOnKeyPressed(KL.getInstance().PRESSED);
    currentScene.setOnKeyReleased(KL.getInstance().RELEASED);

    // scale
    Data.scale.xProperty().bind(currentScene.widthProperty().divide(Data.SCREEN_WIDTH));
    Data.scale.yProperty().bind(currentScene.heightProperty().divide(Data.SCREEN_HEIGHT));
    Data.scale.setPivotX(0);
    Data.scale.setPivotY(0);

    // set root group
    currentScene.setRoot(rootGroup);
  }

  public void updateViewport(int width, int height) {
    Data.scale.xProperty().bind(currentScene.widthProperty().divide(width));
    Data.scale.yProperty().bind(currentScene.heightProperty().divide(height));
  }

  public void changeGameGroup(GameScene nextGameScene, boolean add, boolean removeAll) {

    updateViewport(Data.SCREEN_WIDTH, Data.SCREEN_HEIGHT);

    if (removeAll) {
      rootGroup.getChildren().clear();
      rootGroup.getChildren().add(background);
    }
    if (add) rootGroup.getChildren().add(nextGameScene.myGroup);
    currentGroup = nextGameScene.myGroup;

    // handle updates
    if (currentUpdateTimer != null) currentUpdateTimer.stop();

    currentUpdateTimer = new AnimationTimer() {
      @Override
      public void handle(long now) {
        nextGameScene.update(lastAnimation, now);
        lastAnimation = now;
      }
    };
    currentUpdateTimer.start();

  }

  // TODO: Button Press Deactivation when Transitions are played (multiple presses are possible right now)
  public void changeGameGroupWithAnimation(GameScene nextGroup, int startX, int endX, int startY, int endY) {
    nextGroup.getMyGroup();

    rootGroup.getChildren().add(nextGroup.myGroup);

    TranslateTransition translateTransition = new TranslateTransition(STANDARD_SWITCH_DURATION, nextGroup.myGroup);
    translateTransition.setFromX(startX);
    translateTransition.setToX(endX);
    translateTransition.setFromY(startY);
    translateTransition.setToY(endY);
    translateTransition.setInterpolator(Interpolator.EASE_BOTH);
    translateTransition.setOnFinished(e -> {
      changeGameGroup(nextGroup, false, false);
    });
    translateTransition.play();

    Group tmp = currentGroup;
    TranslateTransition translateTransition2 = new TranslateTransition(STANDARD_SWITCH_DURATION, currentGroup);
    translateTransition2.setFromX(0);
    translateTransition2.setToX(0 - (startX - endX));
    translateTransition2.setFromY(0);
    translateTransition2.setToY(0 - (startY - endY));
    translateTransition2.setInterpolator(Interpolator.EASE_BOTH);
    translateTransition2.setOnFinished(e -> {
      rootGroup.getChildren().remove(tmp);
    });
    translateTransition2.play();

    TranslateTransition grassTranslateTransition = new TranslateTransition(STANDARD_SWITCH_DURATION, background);
    grassTranslateTransition.setFromX(0);
    grassTranslateTransition.setToX(0 - (startX - endX));
    grassTranslateTransition.setFromY(0);
    grassTranslateTransition.setToY(0 - (startY - endY));
    grassTranslateTransition.setInterpolator(Interpolator.EASE_BOTH);
    grassTranslateTransition.setOnFinished(e -> {
      background.setLayoutX(0);
    });
    grassTranslateTransition.play();
  }

  public void changeGameGroupWithLeftAnimation(GameScene nextGroup) {
    currentUpdateTimer.stop();
    changeGameGroupWithAnimation(nextGroup, Data.SCREEN_WIDTH, 0, 0, 0);
  }

  public Stage getCurrentStage() {
    return currentStage;
  }

  public Scene getCurrentScene() {
    return currentScene;
  }

  public void switchOnGrassBackground() {
    // Background
    background.getChildren().clear();
    for (int i = 0; i < (Data.SCREEN_WIDTH / Grass.STANDARD_WIDTH + 1) * 2; i++) {
      for (int j = 0; j < Data.SCREEN_HEIGHT / Grass.STANDARD_HEIGHT + 1; j++) {
        Grass grass = new Grass((int) (i * Grass.STANDARD_WIDTH), (int) (j * Grass.STANDARD_HEIGHT));
        background.getChildren().add(grass.getGroup());
      }
    }
  }

  public void switchOffGrassBackground() {
    background.getChildren().clear();
  }

}
